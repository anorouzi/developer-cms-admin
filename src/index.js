import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import axios from 'axios';

let token = localStorage.getItem('dc-auth-token') || "";
axios.defaults.headers.common['x-auth-token'] = token;

ReactDOM.render(
    <App />
    , document.getElementById('root')
);
serviceWorker.unregister();
