import React, {useEffect} from 'react';
import { Route, Redirect } from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {checkToken} from "./actions/authAction";

const PrivateRoute = ({ component: Component, ...rest }) => {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
    useEffect(()=> {
        dispatch(checkToken());
  },[]);
  return (
    <Route
      {...rest}
      render={ props =>
        !isAuthenticated ? (
          <Redirect to='/' />
        ) : (
           <Component {...props} />
        )
      }
    />
  );
};
export default PrivateRoute;
