import React from 'react';
import './App.scss';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Navbar from "./components/layout/Navbar";
import { Provider } from 'react-redux';
import store from './store';
import PrivateRoute from "./PrivateRoute";

// Pages
import Login from "./components/pages/Login";
import Contents from "./components/pages/Contents";
import Medias from "./components/pages/Medias";
import Messages from "./components/pages/Messages";
import ExtraFields from "./components/pages/ExtraFields";
import NotFound from "./components/pages/NotFound";

function App() {
  return (
      <Provider store={store}>
          <Router>
              <Navbar/>
              <Switch>
                  <Route exact path='/' component={Login} />
                  <PrivateRoute exact path='/contents' component={Contents}/>
                  <PrivateRoute exact path='/medias' component={Medias} />
                  <PrivateRoute exact path='/messages' component={Messages} />
                  <PrivateRoute exact path='/extra-fields' component={ExtraFields} />
                  <Route component={NotFound}/>
              </Switch>
          </Router>
    </Provider>
  );
}
export default App;
