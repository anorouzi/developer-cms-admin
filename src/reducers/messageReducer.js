import { FAIL_DELETE_MESSAGE ,DELETE_MESSAGE,FAIL_GET_MESSAGES,GET_MESSAGES,CLEAR_MESSAGE_STATE } from '../actions/types';

const initialState = {
    items: [],
    failGetMessages : 0,
    failDeleteMessage : 0,
    deletedMessage : 0
};

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_MESSAGES:
            return {
                ...state,
                items: action.payload
            };
        case FAIL_GET_MESSAGES:
            return {
                ...state,
                failGetMessages: state.failGetMessages + 1
            };
        case DELETE_MESSAGE:
            return {
                ...state,
                deletedMessage: state.deletedMessage + 1
            };
        case FAIL_DELETE_MESSAGE:
            return {
                ...state,
                failDeleteMessage: state.failDeleteMessage + 1
            };
        case CLEAR_MESSAGE_STATE:
            return {
                ...state,
                items: [],
                failGetMessages : 0,
                failDeleteMessage : 0,
                deletedMessage : 0
            };
        default:
            return state;
    }
}
