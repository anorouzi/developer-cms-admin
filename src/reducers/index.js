import { combineReducers } from 'redux';
import contentReducer from './contentReducer';
import authReducer from "./authReducer";
import messageReducer from "./messageReducer";
import extraFieldsReducer from "./extraFieldsReducer";
import mediaReducer from "./mediaReducer";

export default combineReducers({
    contents: contentReducer,
    auth : authReducer,
    message : messageReducer,
    extraFields : extraFieldsReducer,
    media : mediaReducer
});
