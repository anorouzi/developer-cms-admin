import {
    GET_CONTENTS,
    FAIL_GET_CONTENTS,
    ADD_CONTENT,
    FAIL_ADD_CONTENT,
    DELETE_CONTENT,
    FAIL_DELETE_CONTENT,
    UPDATE_CONTENT,
    FAIL_UPDATE_CONTENT,
    CLEAR_CONTENT_STATE
} from '../actions/types';

const initialState = {
    items: [],
    failGetContents: 0,
    failAddContent: 0,
    successAddContent: 0,
    failDeleteContent: 0,
    successDeleteContent: 0,
    failUpdateContent: 0,
    successUpdateContent: 0
};

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_CONTENTS:
            return {
                ...state,
                items: action.payload
            };
        case FAIL_GET_CONTENTS:
            return {
                ...state,
                failGetContents: state.failGetContents + 1,
            };
        case ADD_CONTENT:
            return {
                ...state,
                successAddContent: state.successAddContent + 1,
            };
        case FAIL_ADD_CONTENT:
            return {
                ...state,
                failAddContent: state.failAddContent + 1,
            };
        case DELETE_CONTENT:
            return {
                ...state,
                successDeleteContent: state.successDeleteContent + 1,
            };
        case FAIL_DELETE_CONTENT:
            return {
                ...state,
                failDeleteContent: state.failDeleteContent + 1,
            };
        case UPDATE_CONTENT:
            return {
                ...state,
                successUpdateContent: state.successUpdateContent + 1,
            };
        case FAIL_UPDATE_CONTENT:
            return {
                ...state,
                failUpdateContent: state.failUpdateContent + 1,
            };
        case CLEAR_CONTENT_STATE:
            return {
                ...state,
                items: [],
                failGetContents: 0,
                failAddContent: 0,
                successAddContent: 0,
                failDeleteContent: 0,
                successDeleteContent: 0,
                failUpdateContent: 0,
                successUpdateContent: 0
            };
        default:
            return state;
    }
}
