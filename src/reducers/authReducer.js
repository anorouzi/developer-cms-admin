import { SUCCESS_LOGIN,FAIL_LOGIN,VALID_TOKEN,INVALID_TOKEN,LOGOUT } from '../actions/types';

const initialState = {
    isAuthenticated: false,
    failLogin: 0,
    invalidToken : false
};

export default function(state = initialState, action) {
    switch (action.type) {
        case SUCCESS_LOGIN :
            localStorage.setItem('dc-auth-token', action.payload.token);
            return {
                ...state,
                isAuthenticated : true,
                failLogin: state.failLogin,
                invalidToken : false
            };
        case FAIL_LOGIN:
            localStorage.removeItem('dc-auth-token');
            return {
                ...state,
                isAuthenticated : false,
                failLogin: state.failLogin + 1 ,
                invalidToken : false
            };
        case VALID_TOKEN:
            return {
                ...state,
                isAuthenticated : true,
                failLogin: 0,
                invalidToken : false
            };
        case INVALID_TOKEN:
            localStorage.removeItem('dc-auth-token');
            return {
                ...state,
                isAuthenticated : false,
                failLogin: 0,
                invalidToken : true
            };
        case LOGOUT :
            localStorage.removeItem('dc-auth-token');
            return {
                ...state,
                isAuthenticated : false,
                failLogin: 0,
                invalidToken : false
            };
        default:
            return state;
    }
}
