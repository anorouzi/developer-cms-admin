import {
    UPLOAD_MEDIA,
    FAIL_UPLOAD_MEDIA,
    DELETE_MEDIA,
    FAIL_DELETE_MEDIA,
    GET_MEDIAS,
    FAIL_GET_MEDIAS,
    CLEAR_MEDIA_STATE
} from '../actions/types';

const initialState = {
    items: [],
    current : {},
    failGetMedias : 0,
    failDeleteMedia : 0,
    failUploadMedia : 0,
    successDeleteMedia : 0,
    successUploadMedia : 0,
};

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_MEDIAS:
            return {
                ...state,
                items: action.payload
            };
        case FAIL_GET_MEDIAS:
            return {
                ...state,
                failGetMedias: state.failGetMedias + 1
            };
        case DELETE_MEDIA:
            return {
                ...state,
                successDeleteMedia: state.successDeleteMedia + 1
            };
        case FAIL_DELETE_MEDIA:
            return {
                ...state,
                failDeleteMedia: state.failDeleteMedia + 1
            };
        case UPLOAD_MEDIA:
            return {
                ...state,
                successUploadMedia: state.successUploadMedia + 1,
                current : action.payload
            };
        case FAIL_UPLOAD_MEDIA:
            return {
                ...state,
                failUploadMedia: state.failUploadMedia + 1
            };
        case CLEAR_MEDIA_STATE:
            return {
                ...state,
                items: [],
                current : {},
                failGetMedias : 0,
                failDeleteMedia : 0,
                failUploadMedia : 0,
                successDeleteMedia : 0,
                successUploadMedia : 0,
            };
        default:
            return state;
    }
}
