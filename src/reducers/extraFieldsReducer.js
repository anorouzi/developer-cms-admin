import {
    GET_EXTRA_FIELDS,
    FAIL_GET_EXTRA_FIELDS,
    UPDATE_EXTRA_FIELDS,
    FAIL_UPDATE_EXTRA_FIELDS,
    SUCCESS_DELETE_EXTRA_FIELD, FAIL_DELETE_EXTRA_FIELD, CLEAR_EXTRA_FIELD_STATE
} from '../actions/types';

const initialState = {
    items: [],
    failGetExtraFields : 0,
    failUpdateExtraFields : 0,
    successUpdateExtraFields : 0,
    successDeleteExtraField : 0,
    failDeleteExtraField : 0,
};
export default function(state = initialState, action) {
    switch (action.type) {
        case GET_EXTRA_FIELDS:
            return {
                ...state,
                items: action.payload
            };
        case FAIL_GET_EXTRA_FIELDS:
            return {
                ...state,
                failGetExtraFields: state.failGetExtraFields + 1
            };
        case UPDATE_EXTRA_FIELDS:
            return {
                ...state,
                successUpdateExtraFields: state.successUpdateExtraFields + 1
            };
        case FAIL_UPDATE_EXTRA_FIELDS:
            return {
                ...state,
                failUpdateExtraFields: state.failUpdateExtraFields + 1
            };
        case SUCCESS_DELETE_EXTRA_FIELD:
            return {
                ...state,
                successDeleteExtraField: state.successDeleteExtraField + 1
            };
        case FAIL_DELETE_EXTRA_FIELD:
            return {
                ...state,
                failDeleteExtraField: state.failDeleteExtraField + 1
            };
        case CLEAR_EXTRA_FIELD_STATE:
            return {
                ...state,
                items: [],
                failGetExtraFields : 0,
                failUpdateExtraFields : 0,
                successUpdateExtraFields : 0,
                successDeleteExtraField : 0,
                failDeleteExtraField : 0,
            };
        default:
            return state;
    }
}
