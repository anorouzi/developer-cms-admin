import {UPLOAD_MEDIA,FAIL_UPLOAD_MEDIA,DELETE_MEDIA,FAIL_DELETE_MEDIA,GET_MEDIAS,FAIL_GET_MEDIAS,CLEAR_MEDIA_STATE} from './types';
import axios from 'axios';

export const getMedias = () => async dispatch => {
    try {
        const res = await axios.get('/api/medias');
        dispatch({
            type: GET_MEDIAS,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: FAIL_GET_MEDIAS,
        });
    }
};

// Delete Message
export const deleteMedia = (id) => async dispatch => {
    try {
        await axios.delete(`/api/medias/${id}`);
        dispatch({type: DELETE_MEDIA});
    } catch (err) {
        dispatch({type: FAIL_DELETE_MEDIA});
    }
};

// Upload Media
export const addMedia = (obj) => async dispatch => {
    let bodyFormData = new FormData();
    bodyFormData.append('file', obj);
    const config = {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
    };
    try {
       let res = await axios.post('/api/medias',bodyFormData,config);
        dispatch({type: UPLOAD_MEDIA,payload: res.data});
    } catch (err) {
        dispatch({type: FAIL_UPLOAD_MEDIA});
    }
};
// Clear media state
export const clearMediaState = () => dispatch => {
    dispatch({type: CLEAR_MEDIA_STATE});
};
