export const SUCCESS_LOGIN = 'SUCCESS_LOGIN';
export const FAIL_LOGIN = 'FAIL_LOGIN';
export const VALID_TOKEN = 'VALID_TOKEN';
export const INVALID_TOKEN = 'INVALID_TOKEN';
export const LOGOUT = 'LOGOUT';

export const GET_CONTENTS = 'GET_CONTENTS';
export const FAIL_GET_CONTENTS = 'FAIL_GET_CONTENTS';
export const ADD_CONTENT = 'ADD_CONTENT';
export const FAIL_ADD_CONTENT = 'FAIL_ADD_CONTENT';
export const DELETE_CONTENT = 'DELETE_CONTENT';
export const FAIL_DELETE_CONTENT = 'FAIL_DELETE_CONTENT';
export const UPDATE_CONTENT = 'UPDATE_CONTENT';
export const FAIL_UPDATE_CONTENT = 'FAIL_UPDATE_CONTENT';
export const CLEAR_CONTENT_STATE = 'CLEAR_CONTENT_STATE';

export const GET_MESSAGES = 'GET_MESSAGES';
export const FAIL_GET_MESSAGES = 'FAIL_GET_MESSAGES';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const FAIL_DELETE_MESSAGE = 'FAIL_DELETE_MESSAGE';
export const CLEAR_MESSAGE_STATE = 'CLEAR_MESSAGE_STATE';

export const GET_EXTRA_FIELDS = 'GET_EXTRA_FIELDS';
export const FAIL_GET_EXTRA_FIELDS = 'FAIL_GET_EXTRA_FIELDS';
export const UPDATE_EXTRA_FIELDS = 'UPDATE_EXTRA_FIELDS';
export const FAIL_UPDATE_EXTRA_FIELDS = 'FAIL_UPDATE_EXTRA_FIELDS';
export const SUCCESS_DELETE_EXTRA_FIELD = 'SUCCESS_DELETE_EXTRA_FIELD';
export const FAIL_DELETE_EXTRA_FIELD = 'FAIL_DELETE_EXTRA_FIELD';
export const CLEAR_EXTRA_FIELD_STATE = 'CLEAR_EXTRA_FIELD_STATE';

export const UPLOAD_MEDIA = 'UPLOAD_MEDIA';
export const FAIL_UPLOAD_MEDIA = 'FAIL_UPLOAD_MEDIA';
export const DELETE_MEDIA = 'DELETE_MEDIA';
export const FAIL_DELETE_MEDIA = 'FAIL_DELETE_MEDIA';
export const GET_MEDIAS = 'GET_MEDIAS';
export const FAIL_GET_MEDIAS = 'FAIL_GET_MEDIAS';
export const CLEAR_MEDIA_STATE = 'CLEAR_MEDIA_STATE';
