import {SUCCESS_LOGIN, FAIL_LOGIN,VALID_TOKEN,INVALID_TOKEN,LOGOUT} from './types';
import axios from 'axios';

export const createToken = (username,password) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    try {
        const res = await axios.post('/api/check-access', {"username":username,password:password}, config);
        dispatch({
            type: SUCCESS_LOGIN,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: FAIL_LOGIN,
        });
    }
};

export const checkToken = () => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json',
        }
    };
    try {
        const res = await axios.get('/api/check-access', config);
        dispatch({
            type: VALID_TOKEN,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: INVALID_TOKEN,
        });
    }
};

export const logout = () => dispatch => {
    dispatch({
        type: LOGOUT,
    });
};
