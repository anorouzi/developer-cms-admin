import {FAIL_DELETE_MESSAGE ,DELETE_MESSAGE,FAIL_GET_MESSAGES,GET_MESSAGES,CLEAR_MESSAGE_STATE} from './types';
import axios from 'axios';

const config = {
    headers: {
        'Content-Type': 'application/json',
    }
};
export const getMessages = () => async dispatch => {
    try {
        const res = await axios.get('/api/messages', config);
        dispatch({
            type: GET_MESSAGES,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: FAIL_GET_MESSAGES,
        });
    }
};

// Delete Message
export const deleteMessage = (id) => async dispatch => {
    try {
        await axios.delete(`/api/messages/${id}`,config);
        dispatch({type: DELETE_MESSAGE});
    } catch (err) {
        dispatch({type: FAIL_DELETE_MESSAGE});
    }
};
// Clear State
export const clearMessageSate = () => dispatch => {
    dispatch({type: CLEAR_MESSAGE_STATE});
};
