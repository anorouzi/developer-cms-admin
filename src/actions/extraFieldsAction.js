import {
    GET_EXTRA_FIELDS,
    FAIL_GET_EXTRA_FIELDS,
    UPDATE_EXTRA_FIELDS,
    FAIL_UPDATE_EXTRA_FIELDS,
    SUCCESS_DELETE_EXTRA_FIELD, FAIL_DELETE_EXTRA_FIELD, CLEAR_EXTRA_FIELD_STATE
} from './types';
import axios from "axios";

const config = {
    headers: {
        'Content-Type': 'application/json',
    }
};

export const getExtraFields = () => async dispatch => {
    try {
        const res = await axios.get('/api/extra-fields', config);
        dispatch({
            type: GET_EXTRA_FIELDS,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: FAIL_GET_EXTRA_FIELDS,
        });
    }
};
export const updateExtraFields = data => async dispatch => {
    try {
        const res = await axios.post('/api/extra-fields', data, config);
        dispatch({
            type: UPDATE_EXTRA_FIELDS,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: FAIL_UPDATE_EXTRA_FIELDS,
        });
    }
};
export const deleteExtraField = id => async dispatch => {
    try {
        await axios.delete(`/api/extra-fields/${id}`, config);
        dispatch({
            type: SUCCESS_DELETE_EXTRA_FIELD,
        });
    } catch (err) {
        dispatch({
            type: FAIL_DELETE_EXTRA_FIELD,
        });
    }
};
export const clearExtraFieldsState = () => dispatch => {
    dispatch({type:CLEAR_EXTRA_FIELD_STATE})
};
