import {
    GET_CONTENTS,
    FAIL_GET_CONTENTS,
    ADD_CONTENT,
    FAIL_ADD_CONTENT,
    DELETE_CONTENT,
    FAIL_DELETE_CONTENT,
    UPDATE_CONTENT,
    FAIL_UPDATE_CONTENT,
    CLEAR_CONTENT_STATE
} from './types';
import axios from "axios";

// Get Contents
export const getContents = () => async dispatch => {
    try {
        const res = await axios.get('/api/contents/admin');
        dispatch({
            type: GET_CONTENTS,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: FAIL_GET_CONTENTS,
        });
    }
};

// Delete Contents
export const deleteContent = (id) => async dispatch => {
    try {
        await axios.delete(`/api/contents/${id}`);
        dispatch({type: DELETE_CONTENT});
    } catch (err) {
        dispatch({type: FAIL_DELETE_CONTENT});
    }
};

// Add Contents
export const addContent = (formData) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json',
        }
    };
    try {
        await axios.post('/api/contents',formData,config);
        dispatch({type: ADD_CONTENT});
    } catch (err) {
        dispatch({type: FAIL_ADD_CONTENT});
    }
};

// Update Contents
export const updateContent = (formData,id) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache'
        }
    };
    try {
        await axios.put(`/api/contents/${id}`,formData,config);
        dispatch({type: UPDATE_CONTENT});
    } catch (err) {
        dispatch({type: FAIL_UPDATE_CONTENT});
    }
};
export const clearContentState = () => dispatch => {
    dispatch({type:CLEAR_CONTENT_STATE})
};
