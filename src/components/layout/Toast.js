import React, {forwardRef, useImperativeHandle, useState} from "react";

const Toast = forwardRef((props, ref) => {
    const [show, setShow] = useState(false);
    const [toastBody, setToastBody] = useState('');
    const [toastType, setToastType] = useState('');

    useImperativeHandle(ref, () => ({
        showToast(type,msg) {
            setShow(!show);
            setToastType(type);
            setToastBody(msg);
            setTimeout(()=>{
                setShow(false);
            },2500)
        }
    }));
    return (
        <div className={show ? 'toast' + ' ' + toastType + ' active' : 'toast' + ' ' + toastType }>
            <p>{toastBody}</p>
        </div>
    )
});
export default Toast;
