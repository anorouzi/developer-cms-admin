import React, {useEffect, useState} from "react";
import { NavLink} from "react-router-dom";
import { CSSTransition } from 'react-transition-group';
import { useLocation } from 'react-router-dom';
import {logout} from "../../actions/authAction";
import {useDispatch} from "react-redux";

const Navbar = () => {
    const dispatch = useDispatch();
    const [showMenu, setShowMenu] = useState(true);
    useEffect(() => {
        if (window.innerWidth < 768) {
            setShowMenu(false)
        }
    }, []);

    let location = useLocation();
    if (location.pathname !== '/contents' && location.pathname !== '/medias' && location.pathname !== '/messages' && location.pathname !== '/extra-fields') {
        return null
    }
    const onLogout = () => {
        dispatch(logout());
    };
    return (
        <div className='container'>
            { window.innerWidth < 768 && showMenu &&
            <CSSTransition in={showMenu} timeout={0} unmountOnExit classNames="fade-anim">
                    <div className='overlay' onClick={() => setShowMenu(!showMenu)} />
            </CSSTransition>
            }
            <nav className='navbar light'>
                <div className='open-sidenav-btn' onClick={() => setShowMenu(!showMenu)}>
                    <svg className='icon' xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                        <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
                    </svg>
                </div>
                { window.innerWidth < 768 && showMenu && <div className='close-sidenav-btn' onClick={() => setShowMenu(!showMenu)}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>
                </div>}
                <CSSTransition in={showMenu} timeout={1000} unmountOnExit classNames="fade-anim">
                <ul>
                    <li>
                        <div className='top'>
                            <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M4 9h16v2H4V9zm0 4h10v2H4v-2z"/></svg>
                            <span>Developer-CMS</span>
                        </div>
                    </li>
                    <li><NavLink activeClassName='active' to='/contents'>Contents</NavLink></li>
                    <li><NavLink activeClassName='active' to='/messages'>Messages</NavLink></li>
                    <li><NavLink activeClassName='active' to='/medias'>Medias</NavLink></li>
                    <li><NavLink activeClassName='active' to='/extra-fields'>Extra Fields</NavLink></li>
                </ul>
                </CSSTransition>
                <span className='btn logout-btn' onClick={onLogout}>Logout<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"/><path d="M13 3h-2v10h2V3zm4.83 2.17l-1.42 1.42C17.99 7.86 19 9.81 19 12c0 3.87-3.13 7-7 7s-7-3.13-7-7c0-2.19 1.01-4.14 2.58-5.42L6.17 5.17C4.23 6.82 3 9.26 3 12c0 4.97 4.03 9 9 9s9-4.03 9-9c0-2.74-1.23-5.18-3.17-6.83z"/></svg></span>
            </nav>
        </div>
    )
};
export default Navbar
