import React, {forwardRef, Fragment, useImperativeHandle, useState} from "react";
import {CSSTransition} from "react-transition-group";

const Modal = forwardRef((props, ref) => {
    const [show, setShow] = useState(false);
    const [modalBody,setModalBody] = useState('');
    const [modalTitle,setModalTitle] = useState('');
    const [modalFooter,setModalFooter] = useState('');

    useImperativeHandle(ref, () => ({
        showModal(title,body,footer) {
            setShow(!show);
            setModalBody (body);
            setModalTitle(title);
            setModalFooter(footer)
        }
    }));
    return (
        <Fragment>
            <CSSTransition in={show} timeout={1000} unmountOnExit classNames="fade-anim">
            <div className='modal-container'>
                <div className='modal-content'>
                    <div className='modal-header'>
                        {modalTitle}
                        <button className='btn white regular close-modal-btn' onClick={()=>setShow(!show)}>Close</button>
                    </div>
                    <div className='modal-body'>
                        {modalBody}
                    </div>
                    <div className='modal-footer'>
                        {modalFooter}
                    </div>
                </div>
            </div>
            </CSSTransition>
        </Fragment>
    )
});
export default Modal;
