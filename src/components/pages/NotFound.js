import React, {Fragment} from "react";
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";

const NotFound = () => {
    return (
        <Fragment>
            <Helmet>
                <title>404 - Page not found</title>
            </Helmet>
            <div className='not-found'>
                <h1>404</h1>
                <p>This route is not available !</p>
                <Link to='/' className='btn blue regular'>Login</Link>
            </div>

        </Fragment>
    )
};
export default NotFound;
