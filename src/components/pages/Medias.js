import React from "react";
import {CSSTransition} from "react-transition-group";
import {Helmet} from "react-helmet";
import Dropzone from "react-dropzone";
import Toast from "../layout/Toast";
import {connect} from "react-redux";
import {addMedia, clearMediaState, deleteMedia, getMedias} from "../../actions/mediaAction";
import Loading from "../layout/Loading";

class Medias extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showUpload: false,
            failGetMedias: 0,
            failDeleteMedia: 0,
            failUploadMedia: 0,
            successDeleteMedia: 0,
            successUploadMedia: 0,

            itemPerPage: 8,
            currentPage: 1,
            paginationStartIndex: 0,
            paginationEndIndex: 8,

            finishLoading : false,
            uploadMediaLoading : false
        };
        this.toast = React.createRef();
    }

    componentWillMount() {
        this.props.getMedias();
    }
    componentDidMount() {
        setTimeout(()=> {
            this.setState({ finishLoading : true})
        },8000)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.failGetMedias !== this.state.failGetMedias) {
            this.toast.current.showToast('red', 'An error occurred while getting medias.');
            this.setState({failGetMedias: nextProps.failGetMedias})

        }
        if (nextProps.successDeleteMedia !== this.state.successDeleteMedia) {
            this.toast.current.showToast('green', 'The media was successfully deleted.');
            this.setState({successDeleteMedia: nextProps.successDeleteMedia});
            this.props.getMedias();

        }
        if (nextProps.successUploadMedia !== this.state.successUploadMedia) {
            this.toast.current.showToast('green', 'The media was successfully uploaded.');
            this.setState({successUploadMedia: nextProps.successUploadMedia,uploadMediaLoading:false});
            this.props.getMedias();

        }
        if (nextProps.failDeleteMedia !== this.state.failDeleteMedia) {
            this.toast.current.showToast('red', 'An error occurred while deleting media.');
            this.setState({failDeleteMedia: nextProps.failDeleteMedia});

        }
        if (nextProps.failUploadMedia !== this.state.failUploadMedia) {
            this.toast.current.showToast('red', 'An error occurred while uploading media.');
            this.setState({failUploadMedia: nextProps.failUploadMedia});
        }
    }

    componentWillUnmount() {
        this.props.clearMediaState()
    }

    mimeTypeIcon(mime) {
        if (mime.match('image.*')) {
            return <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path
                    d="M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z"/>
            </svg>
        } else if (mime.match('audio.*')) {
            return <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path d="M12 3v10.55c-.59-.34-1.27-.55-2-.55-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4V7h4V3h-6z"/>
            </svg>
        } else if (mime.match('video.*')) {
            return <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path
                    d="M4 6H2v14c0 1.1.9 2 2 2h14v-2H4V6zm16-4H8c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-8 12.5v-9l6 4.5-6 4.5z"/>
            </svg>
        } else if (mime.match('application/x-zip-compressed')) {
            return <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path
                    d="M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z"/>
            </svg>
        } else {
            return <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M0 0h24v24H0z" fill="none"/>
                <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
            </svg>
        }
    };

    bytesToSize(bytes) {
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    };

    copyToClipboard(url) {
        navigator.clipboard.writeText(url).then(() => {
            this.toast.current.showToast('green', 'Copied to clipboard.');
        }, function (err) {
            console.error('Async: Could not copy text: ', err);
        });
    };

    onchangeFile(files) {
        this.setState({uploadMediaLoading:true})
        const fileObj = files[0];
        if (fileObj.size > 104857600) {
            this.toast.current.showToast('red', 'File size must be less than 100 MG.');
        } else if (!fileObj.type.match('image.*') && !fileObj.type.match('audio.*') && !fileObj.type.match('video.*') && !fileObj.type.match('application/x-zip-compressed')) {
            this.toast.current.showToast('red', 'The selected file must be ZIP, Audio, Video or Image');
        } else {
            this.props.addMedia(fileObj);
        }
    };

    onDeleteMedia(uniqueID) {
        this.props.deleteMedia(uniqueID);
    };

    changePage(page) {
        this.setState({
            currentPage: page,
            paginationEndIndex: page * this.state.itemPerPage,
            paginationStartIndex: page * this.state.itemPerPage - this.state.itemPerPage
        });
    }

    pagination() {
        let pagesCount = Math.ceil(this.props.items.length / this.state.itemPerPage);
        let html = [];
        for (let i = 1; i <= pagesCount; i++) {
            html.push(<li key={i} onClick={() => this.changePage(i)}
                          className={this.state.currentPage === i ? 'active' : ''}><span>{i}</span></li>)
        }
        return html
    }

    render() {
        return (
            <div className='container mt-25'>
                <Helmet>
                    <title>Medias</title>
                </Helmet>
                <div className='row'>
                    <div className='col-12'>
                        <CSSTransition in={this.state.showUpload} timeout={400} unmountOnExit classNames="exp-anim">
                            <div className='card' style={{marginBottom: '15px'}}>
                                <div className={ this.state.uploadMediaLoading ? 'choose-media uploading-media-loading' : 'choose-media'}>
                                    <Dropzone onDrop={acceptedFiles => this.onchangeFile(acceptedFiles)}>
                                        {({getRootProps, getInputProps}) => (
                                            <section>
                                                <div style={{padding: '50px 30px'}} {...getRootProps()}>
                                                    <input {...getInputProps()} />
                                                    <p style={{
                                                        marginBottom: '15px',
                                                        fontSize: '15px',
                                                        fontWeight: 600
                                                    }}>Drag & drop some files here, or click to select files</p>
                                                    <p>Zip, Audio, Video & Image (100MB)</p>
                                                </div>
                                            </section>
                                        )}
                                    </Dropzone>
                                </div>
                            </div>
                        </CSSTransition>
                        <div className='card'>
                            <table>
                                <caption>Medias
                                    <button onClick={() => this.setState({showUpload: !this.state.showUpload})}
                                            className='btn outline regular upload-btn'>Upload
                                        New</button>
                                </caption>
                                <thead>
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Upload Date</th>
                                    <th scope="col">Size</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.props.items.slice(this.state.paginationStartIndex, this.state.paginationEndIndex).map(item => (
                                    <tr key={item.uniqueID}>
                                        <td data-label="Name">
                                            <div className='mime-type'>
                                                {this.mimeTypeIcon(item.mimeType)}
                                            </div>
                                            {item.name}
                                        </td>
                                        <td data-label="Time">{new Date(item.uploadDate).toDateString()}</td>
                                        <td data-label="Size">{this.bytesToSize(item.size)}</td>
                                        <td data-label="Action">
                                            <div className='action'>
                                                <span onClick={() => this.onDeleteMedia(item.uniqueID)}
                                                      className="tooltip fade" data-title="Delete"><svg
                                                    xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24"
                                                    width="24"><path
                                                    d="M0 0h24v24H0z" fill="none"/><path
                                                    d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/></svg></span>
                                                <span onClick={() => this.copyToClipboard(item.url)}
                                                      className="tooltip fade" data-title="Get Endpoint"><svg
                                                    xmlns="http://www.w3.org/2000/svg" height="24"
                                                    viewBox="0 0 24 24"
                                                    width="24"><path
                                                    d="M0 0h24v24H0V0z" fill="none"/><path
                                                    d="M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z"/></svg></span>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                            {!this.state.finishLoading && this.props.items.length <= 0 && <div style={{margin:'60px 0 40px',textAlign:'center'}}><Loading/></div>}
                            { this.state.finishLoading && this.props.items.length <= 0 && <p className='no-data'>It seems there is no media.</p>}
                            <ul className='pagination'>
                                {Math.ceil(this.props.items.length / this.state.itemPerPage) > 1 && this.pagination().map(page => page)}
                            </ul>
                        </div>
                    </div>
                </div>
                <Toast ref={this.toast}/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    items: state.media.items.reverse(),
    failGetMedias: state.media.failGetMedias,
    failDeleteMedia: state.media.failDeleteMedia,
    failUploadMedia: state.media.failUploadMedia,
    successDeleteMedia: state.media.successDeleteMedia,
    successUploadMedia: state.media.successUploadMedia
});
export default connect(mapStateToProps, {getMedias, deleteMedia, addMedia,clearMediaState})(Medias);
