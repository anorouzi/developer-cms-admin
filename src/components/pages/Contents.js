import React, {Fragment} from "react";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import {Helmet} from "react-helmet";
import { connect } from 'react-redux';
import {updateContent, getContents, deleteContent, addContent, clearContentState} from '../../actions/contentAction';
import {addMedia, clearMediaState} from "../../actions/mediaAction";
import Loading from "../layout/Loading";
import Toast from "../layout/Toast";
import { animateScroll as scroll } from "react-scroll";
import Dropzone from "react-dropzone";
import {CSSTransition} from "react-transition-group";

class Contents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title : "",
            content : "",
            draft : false,
            thumbnail : "",
            isEdit : false,
            editID : "",
            cleanData : false,
            inputFields : [],
            successUploadQuillMedia : false,

            failGetContents: 0,
            failAddContent: 0,
            successAddContent: 0,
            failDeleteContent: 0,
            successDeleteContent: 0,
            failUpdateContent: 0,
            successUpdateContent: 0,
            failUploadMedia : 0,
            successUploadMedia : 0,

            itemPerPage : 8,
            currentPage : 1,
            paginationStartIndex : 0 ,
            paginationEndIndex : 8,

            finishLoading : false,
            uploadMediaLoading : false,
            quillUploadMediaLoading : false,

            showMediaModal : false
        };
        this.toast = React.createRef();
        this.onChange = this.onChange.bind(this);
        this.onPublish = this.onPublish.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.clearContentState = this.clearContentState.bind(this);

        this.QuillConst = {
            clipboard: {
                matchVisual: false,
            },
            modules: {
                toolbar : {
                    container: [
                        [{'header': [1, 2, 3, 4, 5, 6, false]}],
                        [{'color': []}, {'background': []}],
                        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                        [{'list': 'ordered'}, {'list': 'bullet'},
                            {'indent': '-1'}, {'indent': '+1'}],
                        ['link','video'],
                        ['code-block'],
                        [{'align': []}],
                        ['clean'],
                    ]
                },
            }
        };
        this.onChangeQuill = this.onChangeQuill.bind(this);
        this.QuillMediaHandler = this.QuillMediaHandler.bind(this);
        this.reactQuillRef = null;
    }
    handleAddFields = () => {
        const values = [...this.state.inputFields];
        values.push({key: '', value: ''});
        this.setState({ inputFields : values});
    };
    handleRemoveFields = index => {
        const values = [...this.state.inputFields];
        values.splice(index, 1);
        this.setState({ inputFields : values});
    };
    handleInputChange = (index, event) => {
        const values = [...this.state.inputFields];
        if (event.target.name === "key") {
            values[index].key = event.target.value;
        } else {
            values[index].value = event.target.value;
        }
        this.setState({ inputFields : values});
    };

    componentWillMount() {
        this.props.getContents();
    }
    componentDidMount() {
        setTimeout(()=> {
            this.setState({ finishLoading : true})
        },8000);
    }
    componentWillUnmount() {
        this.props.clearContentState();
        this.props.clearMediaState();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.failGetContents !== this.state.failGetContents) {
            this.toast.current.showToast('red', 'An error occurred while getting contents.');
            this.setState({failGetContents: nextProps.failGetContents})

        }
        if (nextProps.failAddContent !== this.state.failAddContent) {
            this.toast.current.showToast('red', 'An error occurred while adding content.');
            this.setState({failAddContent: nextProps.failAddContent})

        }
        if (nextProps.successAddContent !== this.state.successAddContent) {
            this.toast.current.showToast('green', 'The content was successfully added.');
            this.setState({successAddContent: nextProps.successAddContent});
            this.props.getContents();
            this.clearContentState()

        }
        if (nextProps.failDeleteContent !== this.state.failDeleteContent) {
            this.toast.current.showToast('red', 'An error occurred while deleting content.');
            this.setState({failDeleteContent: nextProps.failDeleteContent})

        }
        if (nextProps.successDeleteContent !== this.state.successDeleteContent) {
            this.toast.current.showToast('green', 'The content was successfully deleted.');
            this.setState({successDeleteContent: nextProps.successDeleteContent});
            this.props.getContents();

        }
        if (nextProps.failUpdateContent !== this.state.failUpdateContent) {
            this.toast.current.showToast('red', 'An error occurred while updating content.');
            this.setState({failUpdateContent: nextProps.failUpdateContent})

        }
        if (nextProps.successUpdateContent !== this.state.successUpdateContent) {
            this.toast.current.showToast('green', 'The content was successfully updated.');
            this.setState({successUpdateContent: nextProps.successUpdateContent});
            this.props.getContents();
            this.clearContentState()

        }
        if (nextProps.failUploadMedia !== this.state.failUploadMedia) {
            this.toast.current.showToast('red', 'An error occurred while uploading thumbnail.');
            this.setState({failUploadMedia: nextProps.failUploadMedia});
        }
        if (nextProps.successUploadMedia !== this.state.successUploadMedia) {
            this.setState({successUploadMedia: nextProps.successUploadMedia,uploadMediaLoading:false});
            if (this.state.showMediaModal) {
                this.setState({quillUploadMediaLoading: false,successUploadQuillMedia:true})
            } else {
                this.toast.current.showToast('green', 'The thumbnail was successfully uploaded.');
            }
        }
        if (nextProps.current !== this.state.thumbnail) {
            if (!this.state.showMediaModal) {
                this.setState({thumbnail: nextProps.current.url});
            }
        }
    }

    onChangeQuill(value) {
        this.setState({content: value });
    }
    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }
    changePage(page) {
        this.setState({ currentPage : page ,paginationEndIndex : page * this.state.itemPerPage ,paginationStartIndex : page * this.state.itemPerPage-this.state.itemPerPage});
    }
    pagination() {
        let pagesCount = Math.ceil(this.props.items.length / this.state.itemPerPage);
        let html = [];
        for (let i = 1; i <= pagesCount; i++) {
            html.push(<li key={i} onClick={() => this.changePage(i)} className={this.state.currentPage === i ? 'active' : ''}><span>{i}</span></li>)
        }
        return html
    }
    onEdit(uniqueID) {
        let item = this.props.items.filter(item => item.uniqueID === uniqueID);
        this.setState({
            title: item[0].title,
            content: item[0].content,
            thumbnail: item[0].thumbnail,
            draft: item[0].draft,
            cleanData: item[0].clean_data,
            inputFields: item[0].extra_fields,
            isEdit: true,
            editID: uniqueID
        });
        scroll.scrollToTop();
    }
    copyToClipboard(uniqueID) {
        navigator.clipboard.writeText(`${document.location.origin}/api/contents/${uniqueID}`).then(() => {
            this.toast.current.showToast('green', 'Copied to clipboard.');
        }, function (err) {
            console.error('Async: Could not copy text: ', err);
        });
    };

    onPublish() {
        if (this.state.title.length > 0 && this.state.content.length > 0) {
            const formData = {
                title : this.state.title,
                content : this.state.content,
                draft : this.state.draft,
                clean_data : this.state.cleanData,
                thumbnail : this.state.thumbnail,
                extra_fields : this.state.inputFields
            };
            if (this.state.cleanData) {
                formData.content = this.reactQuillRef.getEditor().getContents()
            }
            if (this.state.isEdit) {
                this.props.updateContent(formData,this.state.editID)
            } else {
                this.props.addContent(formData)
            }
        } else {
            this.toast.current.showToast('red', 'Title and content cannot be empty.');
        }
    }
    onchangeFile(files) {
        this.setState({uploadMediaLoading:true})
        const fileObj = files[0];
        if (fileObj.size > 10485760) {
            this.toast.current.showToast('red', 'File size must be less than 10 MG.');
        } else if (!fileObj.type.match('image.*')) {
            this.toast.current.showToast('red', 'The selected file must be Video or Image');
        } else {
            this.props.addMedia(fileObj);
        }
    };

    clearContentState() {
        this.setState({title: '', content: '',thumbnail:'', inputFields: [], draft: false,cleanData:false, isEdit: false, editID: ''})
    }

    QuillMediaHandler() {
        let range = this.reactQuillRef.getEditor().getSelection();
        let position = range ? range.index : 0;
        let type ;
        if (this.props.current.mimeType.match('image.*')) type = 'image';
        if (this.props.current.mimeType.match('video.*')) type = 'video';
        this.reactQuillRef.getEditor().insertEmbed(position,type,this.props.current.url);
        this.setState({showMediaModal: false,successUploadQuillMedia:false})
    }
    QuillOnChangeFile (files) {
        this.setState({quillUploadMediaLoading:true});
        const fileObj = files[0];
        if (fileObj.size > 104857600) {
            this.toast.current.showToast('red', 'Media size must be less than 100 MG.');
        } else if (!fileObj.type.match('image.*') && !fileObj.type.match('video.*')) {
            this.toast.current.showToast('red', 'The selected media must be Video or Image');
        } else {
            this.props.addMedia(fileObj);
        }
    }
    render() {
        return (
            <div className='container'>
                <Helmet>
                    <title>Contents</title>
                </Helmet>
                <div className='row'>
                    <div className='col-9' style={{position:'relative'}}>
                        <input value={this.state.title} name='title' onChange={this.onChange} type="text" placeholder=' Title' className='form-input title-input'/>
                        <button className='btn quill-add-media' onClick={()=>this.setState({showMediaModal : !this.state.showMediaModal})}>
                            <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M22 13h-8v-2h8v2zm0-6h-8v2h8V7zm-8 10h8v-2h-8v2zm-2-8v6c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V9c0-1.1.9-2 2-2h6c1.1 0 2 .9 2 2zm-1.5 6l-2.25-3-1.75 2.26-1.25-1.51L3.5 15h7z"/></svg>
                            Add Media</button>
                        <ReactQuill ref={(el) => { this.reactQuillRef = el }} style={{zIndex:3,position:'relative'}}  modules={this.QuillConst.modules} onChange={this.onChangeQuill} value={this.state.content} theme="snow"/>
                        { this.state.inputFields.length > 0 &&
                        <div className='card extra-fields-content-container'>
                            {this.state.inputFields.map((inputField, index) => (
                                <Fragment key={`${inputField}~${index}`}>
                                    <div className="row">
                                        <div className="col-4">
                                            <input
                                                placeholder='key'
                                                type="text"
                                                className="form-input"
                                                id="key"
                                                name="key"
                                                value={inputField.key}
                                                onChange={event => this.handleInputChange(index, event)}
                                            />
                                        </div>
                                        <div className="col-8">
                                            <input
                                                placeholder='value'
                                                type="text"
                                                className="form-input"
                                                id="value"
                                                name="value"
                                                value={inputField.value}
                                                onChange={event => this.handleInputChange(index, event)}
                                            />
                                        </div>
                                        <div className="btn white regular remove-field-btn" onClick={() => this.handleRemoveFields(index)}>Remove</div>
                                    </div>
                                </Fragment>
                            ))}
                        </div>
                        }
                        <div className='card extra-field' onClick={() => this.handleAddFields()}>
                            <span className="tooltip fade" data-title="Extra field">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 443.733 443.733"><path d="M187.733 0H17.067C7.641 0 0 7.641 0 17.067v170.667c0 9.426 7.641 17.067 17.067 17.067h170.667c9.426 0 17.067-7.641 17.067-17.067V17.067C204.8 7.641 197.159 0 187.733 0zm-17.066 170.667H34.133V34.133h136.533v136.534zM426.667 0H256c-9.426 0-17.067 7.641-17.067 17.067v170.667c0 9.426 7.641 17.067 17.067 17.067h170.667c9.426 0 17.067-7.641 17.067-17.067V17.067C443.733 7.641 436.092 0 426.667 0zM409.6 170.667H273.067V34.133H409.6v136.534zM187.733 238.933H17.067C7.641 238.933 0 246.574 0 256v170.667c0 9.426 7.641 17.067 17.067 17.067h170.667c9.426 0 17.067-7.641 17.067-17.067V256c-.001-9.426-7.642-17.067-17.068-17.067zM170.667 409.6H34.133V273.067h136.533V409.6zM426.667 324.267H358.4V256c0-9.426-7.641-17.067-17.067-17.067s-17.067 7.641-17.067 17.067v68.267H256c-9.426 0-17.067 7.641-17.067 17.067S246.574 358.4 256 358.4h68.267v68.267c0 9.426 7.641 17.067 17.067 17.067s17.067-7.641 17.067-17.067V358.4h68.267c9.426 0 17.067-7.641 17.067-17.067s-7.643-17.066-17.068-17.066z"/></svg>
                            </span>
                        </div>
                    </div>
                    <div className='col-3'>
                        <div className='card' style={{position:'sticky',top:'10px'}}>
                            {this.state.isEdit && <button className='btn cancel-edit regular' onClick={this.clearContentState}>Cancel Editing</button>}
                            <div className={ this.state.uploadMediaLoading ? 'choose-media uploading-media-loading' : 'choose-media'}>
                                { this.state.thumbnail && <div className='cover' style={{"--bg-image": 'url('+this.state.thumbnail+')'}} /> }
                                <Dropzone onDrop={acceptedFiles => this.onchangeFile(acceptedFiles)}>
                                    {({getRootProps, getInputProps}) => (
                                            <div style={{padding: '50px 15px'}} {...getRootProps()}>
                                                <input {...getInputProps()} />
                                                <p className='text'>Choose a thumbnail</p>
                                            </div>
                                    )}
                                </Dropzone>
                            </div>
                            <div className='switch-wrapper' >
                                <div>
                                    <span>Draft</span>
                                    <input checked={this.state.draft} type="checkbox" onChange={()=>this.setState({draft:!this.state.draft})} id='draft' className="switch"/>
                                    <label htmlFor="draft" />
                                </div>
                                <div>
                                    <span>Clean data</span>
                                    <input checked={this.state.cleanData} type="checkbox" onChange={()=>this.setState({cleanData:!this.state.cleanData})} id='cleanData' className="switch"/>
                                    <label htmlFor="cleanData" />
                                </div>
                            </div>
                            <a href="#" className='btn blue block publish' onClick={this.onPublish}>{this.state.draft ? 'Draft' : 'Publish' }</a>
                        </div>
                    </div>
                </div>
                <div className='row mt-25'>
                    <div className='col-12'>
                        <div className='card'>
                            <table>
                                <caption>Your posts</caption>
                                <thead>
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Create date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.props.items.slice(this.state.paginationStartIndex,this.state.paginationEndIndex).map(content => (
                                    <tr key={content.uniqueID}>
                                    <td data-label="Title">{content.title}</td>
                                    <td data-label="Time">{new Date(content.date).toDateString()}</td>
                                    <td data-label="Status">{content.draft ? "Draft" : "Publish"}</td>
                                    <td data-label="Action">
                                        <div className='action'>
                                             <span className="tooltip fade" data-title="Edit" onClick={()=> this.onEdit(content.uniqueID)}><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/></svg></span>
                                             <span className="tooltip fade" data-title="Delete" onClick={()=>this.props.deleteContent(content.uniqueID)}><svg xmlns="http://www.w3.org/2000/svg" height="24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"/></svg></span>
                                             <span className="tooltip fade" data-title="Get Endpoint" onClick={()=>this.copyToClipboard(content.uniqueID)}><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z"/></svg></span>
                                        </div>
                                    </td>
                                    </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                            {!this.state.finishLoading && this.props.items.length <= 0 && <div style={{margin:'60px 0 0',textAlign:'center'}}><Loading/></div>}
                            { this.state.finishLoading && this.props.items.length <= 0 && <p className='no-data'>It seems there is no content.</p>}
                            <ul className='pagination'>
                                {Math.ceil(this.props.items.length / this.state.itemPerPage) > 1 && this.pagination().map(page => page)}
                            </ul>
                        </div>
                    </div>
                </div>
                <Toast ref={this.toast} />
                <CSSTransition in={this.state.showMediaModal} timeout={1000} unmountOnExit classNames="fade-anim">
                    <div className='modal-container'>
                        <div className='modal-content'>
                            <div className='modal-header'>
                               Add media
                                <button className='btn white regular close-modal-btn' onClick={()=>this.setState({showMediaModal : !this.state.showMediaModal})}>Close</button>
                            </div>
                            <div className='modal-body'>
                                <div className={ this.state.quillUploadMediaLoading ? 'choose-media uploading-media-loading' : 'choose-media'}>
                                    {!this.state.successUploadQuillMedia ?
                                    <Dropzone onDrop={acceptedFiles => this.QuillOnChangeFile(acceptedFiles)}>
                                        {({getRootProps, getInputProps}) => (
                                            <section>
                                                <div style={{padding: '50px 30px'}} {...getRootProps()}>
                                                    <input {...getInputProps()} />
                                                    <p style={{
                                                        marginBottom: '15px',
                                                        fontSize: '15px',
                                                        fontWeight: 600
                                                    }}>Drag & drop some files here, or click to select files</p>
                                                    <p>Video & Image (100MB)</p>
                                                </div>
                                            </section>
                                        )}
                                    </Dropzone> :
                                        <div className='quill-success-upload-media'><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M18 7l-1.41-1.41-6.34 6.34 1.41 1.41L18 7zm4.24-1.41L11.66 16.17 7.48 12l-1.41 1.41L11.66 19l12-12-1.42-1.41zM.41 13.41L6 19l1.41-1.41L1.83 12 .41 13.41z"/></svg><p>The media was successfully uploaded. You can now add it to your content.</p></div>
                                    }
                                </div>
                            </div>
                            <div className='modal-footer'>
                                {this.state.successUploadQuillMedia ? <button className='btn regular dark' onClick={()=>this.QuillMediaHandler()}>Insert</button> :
                                    <button disabled className='btn regular disable'>Insert</button>}

                            </div>
                        </div>
                    </div>
                </CSSTransition>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    items: state.contents.items,
    failGetContents: state.contents.failGetContents,
    failAddContent: state.contents.failAddContent,
    successAddContent: state.contents.successAddContent,
    failDeleteContent: state.contents.failDeleteContent,
    successDeleteContent: state.contents.successDeleteContent,
    failUpdateContent: state.contents.failUpdateContent,
    successUpdateContent: state.contents.successUpdateContent,

    failUploadMedia : state.media.failUploadMedia,
    successUploadMedia : state.media.successUploadMedia,
    current : state.media.current,
});
export default connect(mapStateToProps, { addMedia,updateContent, getContents,deleteContent,addContent,clearContentState,clearMediaState  })(Contents);
