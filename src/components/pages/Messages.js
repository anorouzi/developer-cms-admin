import React from "react";
import Modal from "../layout/Modal";
import {Helmet} from "react-helmet";
import {connect} from "react-redux";
import {clearMessageSate, deleteMessage, getMessages} from "../../actions/messageAction";
import Toast from "../layout/Toast";
import Loading from "../layout/Loading";

class Messages extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            deletedMessage : 0,
            failGetMessages : 0,
            failDeleteMessage : 0,

            itemPerPage : 8,
            currentPage : 1,
            paginationStartIndex : 0 ,
            paginationEndIndex : 8,

            finishLoading : false,
        };
        this.modal = React.createRef();
        this.toast = React.createRef();
    }
    componentWillMount() {
        this.props.getMessages();
    }
    componentDidMount() {
        setTimeout(()=> {
            this.setState({ finishLoading : true})
        },8000)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.failGetMessages !== this.state.failGetMessages) {
            this.toast.current.showToast('red','An error occurred while getting messages.');
            this.setState({ failGetMessages: nextProps.failGetMessages })

        } if (nextProps.deletedMessage !== this.state.deletedMessage ) {
            this.toast.current.showToast('green','The message was successfully deleted.');
            this.setState({ deletedMessage:nextProps.deletedMessage });
            this.props.getMessages();

        } if (nextProps.failDeleteMessage !== this.state.failDeleteMessage) {
            this.toast.current.showToast('red','An error occurred while deleting message.');
            this.setState({ failDeleteMessage:nextProps.failDeleteMessage });
        }
    }

    componentWillUnmount() {
        this.props.clearMessageSate()
    }

    changePage(page) {
        this.setState({ currentPage : page ,paginationEndIndex : page * this.state.itemPerPage ,paginationStartIndex : page * this.state.itemPerPage-this.state.itemPerPage});
    }
    pagination() {
        let pagesCount = Math.ceil(this.props.items.length / this.state.itemPerPage);
        let html = [];
        for (let i = 1; i <= pagesCount; i++) {
            html.push(<li key={i} onClick={() => this.changePage(i)} className={this.state.currentPage === i ? 'active' : ''}><span>{i}</span></li>)
        }
        return html
    }
    render() {
        return (
            <div className='container mt-25'>
                <Helmet>
                    <title>Messages</title>
                </Helmet>
                <div className='row'>
                    <div className='col-12'>
                        <div className='card'>
                            <table>
                                <caption>Received Messages</caption>
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                { this.props.items.slice(this.state.paginationStartIndex,this.state.paginationEndIndex).map(item => (
                                    <tr key={item.uniqueID}>
                                        <td data-label="Name">{item.name}</td>
                                        <td data-label="Email">{item.email}</td>
                                        <td data-label="Time">{new Date(item.date).toDateString()}</td>
                                        <td data-label="Action">
                                            <div className='action'>
                                <span className="tooltip fade" data-title="See"
                                      onClick={() => this.modal.current.showModal(item.name, item.message, `IP Address : ${item.ip}`)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path
                                        d="M0 0h24v24H0z" fill="none"/><path
                                        d="M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z"/></svg>
                                </span>
                                                <span className="tooltip fade" data-title="Delete" onClick={()=>this.props.deleteMessage(item.uniqueID)}><svg
                                                    xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24"
                                                    width="24"><path
                                                    d="M0 0h24v24H0z" fill="none"/><path
                                                    d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z"/></svg></span>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                            {!this.state.finishLoading && this.props.items.length <= 0 && <div style={{margin:'60px 0 40px',textAlign:'center'}}><Loading/></div>}
                            { this.state.finishLoading && this.props.items.length <= 0 && <p className='no-data'>It seems there is no message.</p>}
                            <ul className='pagination'>
                                {Math.ceil(this.props.items.length / this.state.itemPerPage) > 1 && this.pagination().map(page => page)}
                            </ul>
                        </div>
                        <Modal ref={this.modal}/>
                    </div>
                </div>
                <Toast ref={this.toast} />
            </div>
        )
    }
}
const mapStateToProps = state => ({
    items: state.message.items.reverse(),
    failGetMessages: state.message.failGetMessages,
    failDeleteMessage: state.message.failDeleteMessage,
    deletedMessage: state.message.deletedMessage,
});
export default connect(mapStateToProps, { getMessages,deleteMessage,clearMessageSate })(Messages);
