import React, {Fragment} from "react";
import {Helmet} from "react-helmet";
import {connect} from "react-redux";
import {
    clearExtraFieldsState,
    deleteExtraField,
    getExtraFields,
    updateExtraFields
} from "../../actions/extraFieldsAction";
import Toast from "../layout/Toast";

class ExtraFields extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputFields : [{key:'',value:''}],
            failGetExtraFields: 0,
            failUpdateExtraFields: 0,
            successUpdateExtraFields: 0,
            failDeleteExtraField: 0,
            successDeleteExtraField: 0
        };
        this.toast = React.createRef();
    }
    componentWillMount() {
        this.props.getExtraFields();
    }
    componentWillUnmount() {
        this.props.clearExtraFieldsState()
    }
    componentWillReceiveProps(nextProps) {

         if (nextProps.failGetExtraFields !== this.state.failGetExtraFields ) {
            this.toast.current.showToast('red','There was a problem when receiving extra fields.');
             this.setState({failGetExtraFields : nextProps.failGetExtraFields })

        } if (nextProps.failUpdateExtraFields !== this.state.failUpdateExtraFields ) {
            this.toast.current.showToast('red','There was a problem when updating extra fields.');
            this.setState({failUpdateExtraFields : nextProps.failUpdateExtraFields })

        } if (nextProps.successUpdateExtraFields !== this.state.successUpdateExtraFields ) {
            this.toast.current.showToast('green','Extra fields were successfully updated');
            this.setState({successUpdateExtraFields : nextProps.successUpdateExtraFields });
            this.props.getExtraFields();

        } if (nextProps.failDeleteExtraField !== this.state.failDeleteExtraField ) {
            this.toast.current.showToast('red','There was a problem when extra additional fields.');
            this.setState({failDeleteExtraField : nextProps.failDeleteExtraField })

        } if (nextProps.successDeleteExtraField !== this.state.successDeleteExtraField ) {
            this.toast.current.showToast('green','Extra fields were successfully removed.');
            this.setState({successDeleteExtraField : nextProps.successDeleteExtraField });
            this.props.getExtraFields();
        }
        if (nextProps.items !== this.state.inputFields) {
            this.setState({inputFields : nextProps.items });
        }
    }
    handleAddFields = (e) => {
        e.preventDefault();
        const values = [...this.state.inputFields];
        values.push({key: '', value: ''});
        this.setState({ inputFields : values});
    };
    handleRemoveFields = index => {
        const values = [...this.state.inputFields];
        values.splice(index, 1);
        this.setState({ inputFields : values});

        const uniqueID = this.state.inputFields[index].uniqueID;
        if (uniqueID !== undefined) {
            this.props.deleteExtraField(uniqueID);
        }
    };
    handleInputChange = (index, event) => {
        const values = [...this.state.inputFields];
        if (event.target.name === "key") {
            values[index].key = event.target.value;
        } else {
            values[index].value = event.target.value;
        }
        this.setState({ inputFields : values});
    };
    handleSubmit = e => {
        e.preventDefault();
        this.props.updateExtraFields({inputFields:this.state.inputFields});
    };
    render() {
        return (
            <div className='container mt-25'>
                <Helmet>
                    <title>Extra Fields</title>
                </Helmet>
                <div className='row'>
                    <div className='col-12'>
                        <div className='card extra-fields-container'>
                            <h4>Extra field</h4>
                            <form onSubmit={this.handleSubmit}>
                                {this.state.inputFields.map((inputField, index) => (
                                    <Fragment key={`${inputField}~${index}`}>
                                        <div className="row">
                                            <div className="col-4">
                                                <input
                                                    placeholder='key'
                                                    type="text"
                                                    className="form-input"
                                                    id="key"
                                                    name="key"
                                                    value={inputField.key}
                                                    onChange={event => this.handleInputChange(index, event)}
                                                />
                                            </div>
                                            <div className="col-8">
                                                <input
                                                    placeholder='value'
                                                    type="text"
                                                    className="form-input"
                                                    id="value"
                                                    name="value"
                                                    value={inputField.value}
                                                    onChange={event => this.handleInputChange(index, event)}
                                                />
                                            </div>
                                            <div className="btn white regular remove-field-btn" onClick={() => this.handleRemoveFields(index)}>Remove</div>
                                        </div>
                                    </Fragment>
                                ))}
                                <button className='mt-25 btn blue regular' type='submit'>
                                    Save Fields
                                </button>
                                <button className="btn outline regular" onClick={this.handleAddFields}>Add New</button>
                            </form>
                        </div>
                    </div>
                </div>
                <Toast ref={this.toast} />
            </div>
        )
    }
}
const mapStateToProps = state => ({
    items : state.extraFields.items,
    failGetExtraFields: state.extraFields.failGetExtraFields,
    failUpdateExtraFields: state.extraFields.failUpdateExtraFields,
    successUpdateExtraFields: state.extraFields.successUpdateExtraFields,
    failDeleteExtraField: state.extraFields.failDeleteExtraField,
    successDeleteExtraField: state.extraFields.successDeleteExtraField
});
export default connect(mapStateToProps, { getExtraFields,updateExtraFields,deleteExtraField,clearExtraFieldsState })(ExtraFields);
